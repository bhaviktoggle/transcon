<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
date_default_timezone_set("Asia/Calcutta");   //India time (GMT+5:30)
class HomeModal extends CI_Model {

   function __construct() {
    	parent::__construct();
   	$this->load->library('session');
   }

   function EnquireForm($result){
   	$result = $this->input->post();
   	extract($result);
   	
		$created_date = $date = date('Y-m-d H:i:s');
		$result['created_date'] = $created_date;
		$result['ip'] = $this->input->ip_address();
		$query = $this->db->insert('enquire', $result);
		$check = $this->db->affected_rows();
		
		if($check == 1){
			/*Start Email*/				
				$this->load->library('phpmailer_lib');
				$mail = $this->phpmailer_lib->load();
				// SMTP configuration
				define('SMTP_HOST','relay-hosting.secureserver.net');
				define('SMTP_PORT',25);
				define('SMTP_AUTH',true);
				// $mail = new PHPMailer();
				// $mail->IsSMTP(); //do not uncomment this line
				//$mail->SMTPSecure = 'ssl';
				$mail -> SMTPDebug = 1;
				$mail->Host = "smtpout.secureserver.net";
				$mail->SMTPAuth = SMTP_AUTH;
				$mail->Port = 80;
				$mail->Username = "Luminati@transcon.in";
				$mail->Password = "notavailable";
				$mail->SetFrom('Luminati@transcon.in', 'Transcon Triumph');
				$mail->AddReplyTo("Luminati@transcon.in","Transcon Triumph");
				$mail->Subject = 'Enquiry from Transcon Triumph';
				if (empty($enquire_location) && empty($enquire_message)) {
					$mail->Body = 'Hi Team Luminati,<br/><br/> You have the following enquiry :<br/><br/>'
					.'Name: '.$enquire_name.'<br/>'
					.'Phone number: '.$enquire_phone.'<br/>'
					.'Email ID: '.$enquire_email.'<br/><br/>'
					.'Thank you.';
				}
				else {
					$mail->Body = 'Hi Team Luminati,<br/><br/> You have the following enquiry :<br/><br/>'
					.'Name: '.$enquire_name.'<br/>'
					.'Phone number: '.$enquire_phone.'<br/>'
					.'Email ID: '.$enquire_email.'<br/>'
					.'Location: '.$enquire_location.'<br/>'
					.'Message: '.$enquire_message.'<br/><br/>'
					.'Thank you.';
				}
				
				$mail->IsHTML(true);
				// $mail->AddAddress($result['enquire_email']);
				$mail->AddAddress('Luminati@transcon.in');
				// $mail->AddAddress('bhavik@togglehead.in');
				// $mail->AddBCC('sanjay@togglehead.in,pratikmistry@togglehead.in,bhavik@togglehead.in');
				// $mail->AddCC('sanjay@togglehead.in');
				// $mail->AddCC('pratikmistry@togglehead.in');
				// $mail->AddCC('bhavik@togglehead.in');
				if(!$mail->Send()) {
					$msg = ['status' => 'error', 'msg' => 'Something Wrong'];

				} else {
					$_SESSION["check_session"] = "thankyou";
					$msg = ['status' => 'true', 'msg' => 'Thank you for expressing interest on Transcon Developers. Our team will get in touch with you shortly.'];
				}
			/*End Email*/
		}else{
			$msg = ['status' => 'error', 'msg' => 'Server error'];
		}
		echo json_encode($msg);
   }
}