<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$page = 'thankyou';
?>
<!DOCTYPE html>
<html>
   	<head>
      	<title>Thank You</title>
      	<?php include("common/meta-content.php");?>
      	<?php include("common/css-scripts.php");?>
      	<style type="text/css">
      		.error{color: red;}
      	</style>
   	</head>
   	<body>
      	<!-- WEBSITE HEADER STARTS HERE -->
      	<?php include("common/header.php");?>
      	<!-- WEBSITE HEADER ENDS HERE -->

      	<!-- PAGE CONTENT STARTS HERE -->
      		<h1>THANK YOU</h1>
      	<!-- PAGE CONTENT ENDS HERE -->

      	<!-- WEBSITE FOOTER STARTS HERE -->
      	<?php include("common/footer.php");?>
      	<!-- WEBSITE FOOTER ENDS HERE -->
      	<?php include("common/js-scripts.php");?>
   	</body>
</html>