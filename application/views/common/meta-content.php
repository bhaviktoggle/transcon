<!-- META CONTENT   -->

<title></title>

<meta charset="utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1.0">

<meta property="fb:app_id" content="" />

<!-- specification for IE 11 browser -->

<meta http-equiv="X-UA-Compatible" content="IE=edge" />

<!-- specification for IE 10 browser -->

<meta http-equiv="X-UA-Compatible" content="IE=100" />

        <!-- Location identification -->

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link rel="shortcut icon" href="" type="image/gif" />

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

<meta name="description" content="" />

<meta name="abstract" content="" />

<meta name="keywords" content="" />

<meta name="robots" content="follow, index" />

<meta name="news_keywords" content="" />

<meta name="standout" content="" />

<meta name="generator" content="" />

<link rel="canonical" href="" />

<link rel="shortlink" href="" />
<link rel="icon" href="<?php echo base_url(); ?>assets/images/favicon.jpg" type="image/svg" sizes="16x16">
<!-- META CONTENT ENDS -->