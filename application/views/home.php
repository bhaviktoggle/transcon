<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$page = 'home';
?>
<!DOCTYPE html>
<html>
   	<head>
      	<title>Transcon Triumph</title>
      	<?php include("common/meta-content.php");?>
      	<?php include("common/css-scripts.php");?>
      	<style type="text/css">
      		.error{color: red;}
      	</style>
   	</head>
   	<body>
   		<input type="hidden" id="base_url" name="base_url" value="<?php echo base_url(); ?>" >
   		<input type="hidden" id="clickedPDF" name="clickedPDF" value="">
      	<!-- WEBSITE HEADER STARTS HERE -->
      	<?php include("common/header.php");?>
      	<!-- WEBSITE HEADER ENDS HERE -->

      	<!-- PAGE CONTENT STARTS HERE -->
        
      	<section id="section1" class="section1">
      		<div class="my_container">
      			<div class="row">
      				<div class="logo">
      					<img src="<?php echo base_url(); ?>assets/images/logo.png" class="img-fluid">
      				</div>
      			</div>
      			<div class="row">
      				<div class="col-lg-6">
      					<div class="banner_title d-block d-lg-none">
      						<div class="bannertitleline1">
      							Introducing
      						</div>
      						<div class="bannertitleline2">
      							Limited Edition Luxury
      						</div>
      						<div class="bannertitleline3">
      							Andheri (W)
      						</div>
      					</div>
      					<div class="top_banner">
      						<img src="<?php echo base_url(); ?>assets/images/banner1.png" class="img-fluid">
      					</div>
      				</div>
      				<div class="col-lg-6">
      					<div class="banner_details">
      						<div class="banner_title d-none d-lg-block">
      							<div class="bannertitleline1">
      								Introducing
      							</div>
      							<div class="bannertitleline2">
      								Limited Edition Luxury
      							</div>
      							<div class="bannertitleline3">
      								Andheri (W)
      							</div>
      						</div>
      						<div class="banner_desc">
      							OC Received | 4 BHK Palatial Sky Homes<br> 2493 sq. ft.* Carpet Area
      						</div>
      						<div class="banner_address">
      							Luminati<br> Transcon Triumph<!-- <br> Andheri (W) -->
      						</div>
      					</div>
      				</div>
      				<div class="col-12">
      					<div class="text-center scrolltosection2">
      						<a href="#section2" class="scrolls"><i class="fa fa-2x fa-angle-down"></i></a>
      					</div>
      				</div>
      			</div>
      		</div>
      	</section>

      	<section id="section2" class="section2">
      		<div class="my_container">
      			<div class="row align-items-center">
      				<div class="col-lg-4">
      					<div class="section2_data">
      						<div class="page_title_box">
      							<div class="page_subtitle">
      								<img src="<?php echo base_url(); ?>assets/images/leaf.svg" class="img-fluid">
      								<span>apartments</span>
      							</div>
      							<div class="page_title">
      								Exquisitely designed living experience
      							</div>
      						</div>

      						<div class="section2_flattype">
      							4 & 5 bhk
      						</div>
      						<div class="section2banner d-block d-lg-none">
      							<img src="<?php echo base_url(); ?>assets/images/section2banner.jpg" class="img-fluid">
      						</div>

      						<div class="section2_byline">
      							Luminati by Transcon
      						</div>
      						<div class="section2_desc">
      							Each residence at Luminati is planned with  perfection to maximize your living space. From  unique design to quality fitments, from elegant  settings to flawless finishing, every little thing has  been taken into consideration to meet your  discerning style statement.
      						</div>
      					</div>
      				</div>
      				<div class="col-lg-8">
      					<div class="section2banner d-none d-lg-block">
      						<img src="<?php echo base_url(); ?>assets/images/section2banner.jpg" class="img-fluid">
      					</div>
      				</div>
      			</div>
      		</div>
      	</section>

      	<section id="section3" class="section3">
      		<div class="my_container">
      			<div class="page_title_box">
      				<div class="page_subtitle">
      					<img src="<?php echo base_url(); ?>assets/images/leaf.svg" class="img-fluid">
      					<span>views</span>
      				</div>
      				<div class="page_title">
      					Panoramic views as distinctive as your vision
      				</div>
      			</div>
      			<div class="panoramic_view_slider">
      				<div class="panoramic_view_swiper swiper-container">
      					<div class="swiper-wrapper">
      						<div class="swiper-slide">
      							<div class="panoramic_view_img">
      								<img src="<?php echo base_url(); ?>assets/images/pan1.jpg" class="img-fluid">
      							</div>
      						</div>
      						<div class="swiper-slide">
      							<div class="panoramic_view_img">
      								<img src="<?php echo base_url(); ?>assets/images/pan2.jpg" class="img-fluid">
      							</div>
      						</div>
      					</div>
      					<div class="swiper-pagination"></div>
      				</div>
      			</div>
      		</div>
      	</section>

      	<section id="section4" class="section4">
      		<div class="my_container">
      			<div class="page_title_box text-center">
      				<div class="page_subtitle">
      					<img src="<?php echo base_url(); ?>assets/images/leaf.svg" class="img-fluid">
      					<span>PROJECT WALKTHROUGH</span>
      				</div>
      				<div class="page_title">
      					Explore limited edition luxury
      				</div>
      			</div>
      			<div class="project_walkthrough_video">
      				<div id="videocover" class="videocover text-center">
      					<img src="<?php echo base_url(); ?>assets/images/walkthrough.jpg" class="img-fluid d-none d-sm-block">
      					<img src="<?php echo base_url(); ?>assets/images/walkthroughmob.jpg" class="img-fluid d-block d-sm-none">
			       		<img onclick="playVideo();" src="<?php echo base_url(); ?>assets/images/playicon.svg" class="img-fluid playicon">
			        </div>
      				<iframe id="walkthroughvideo" width="560" height="315" src="https://www.youtube.com/embed/HQ5RJuzXLJA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      			</div>
      		</div>
      	</section>

      	<section id="section5" class="section5">
      		<div class="my_container">
      			<div class="row align-items-center">
      				<div class="col-lg-4">
      					<div class="page_title_box">
      						<div class="page_subtitle white">
      							<img src="<?php echo base_url(); ?>assets/images/wleaf.svg" class="img-fluid">
      							<span>AMENITIES</span>
      						</div>
      						<div class="page_title white">
      							Reserved exclusive indulgences
      						</div>
      					</div>
      					<div class="section5_desc white">
      						Explore the finest recreational amenities at Luminati that are truly limited edition. Be it the extravagant clubhouse or the luxurious pool, you are sure to experience grandness in both style and stature.
      					</div>
      				</div>
      				<div class="col-lg-8">
      					<div class="amenities_slider">
      						<div class="amenities_swiper swiper-container">
      							<div class="swiper-wrapper">
      								<div class="swiper-slide">
      									<div class="amenities_data">
      										<div class="amenities_img_box">
      											<div class="amenities_img_icon d-none d-lg-block">
      												<img src="<?php echo base_url(); ?>assets/images/amenities/icon/banquethall.svg" class="img-fluid">
      											</div>
      											<div class="amenities_name d-block d-lg-none">
      												banquet hall
      											</div>
      											<div class="amenities_img">
      												<img src="<?php echo base_url(); ?>assets/images/amenities/image/banquethall.jpg" class="img-fluid">
      											</div>
      										</div>
      										<div class="amenities_img_icon d-block d-lg-none">
      											<img src="<?php echo base_url(); ?>assets/images/amenities/icon/banquethall.svg" class="img-fluid">
      										</div>
      										<div class="amenities_name d-none d-lg-block">
      											banquet hall
      										</div>
      									</div>
      								</div>
      								<div class="swiper-slide">
      									<div class="amenities_data">
      										<div class="amenities_img_box">
      											<div class="amenities_img_icon d-none d-lg-block">
      												<img src="<?php echo base_url(); ?>assets/images/amenities/icon/cafelounge.svg" class="img-fluid">
      											</div>
      											<div class="amenities_name d-block d-lg-none">
      												cafe lounge
      											</div>
      											<div class="amenities_img">
      												<img src="<?php echo base_url(); ?>assets/images/amenities/image/cafelounge.jpg" class="img-fluid">
      											</div>
      										</div>
      										<div class="amenities_img_icon d-block d-lg-none">
      											<img src="<?php echo base_url(); ?>assets/images/amenities/icon/cafelounge.svg" class="img-fluid">
      										</div>
      										<div class="amenities_name d-none d-lg-block">
      											cafe lounge
      										</div>
      									</div>
      								</div>
      								<div class="swiper-slide">
      									<div class="amenities_data">
      										<div class="amenities_img_box">
      											<div class="amenities_img_icon d-none d-lg-block">
      												<img src="<?php echo base_url(); ?>assets/images/amenities/icon/coworkingspace.svg" class="img-fluid">
      											</div>
      											<div class="amenities_name d-block d-lg-none">
      												co-working space
      											</div>
      											<div class="amenities_img">
      												<img src="<?php echo base_url(); ?>assets/images/amenities/image/coworkingspace.jpg" class="img-fluid">
      											</div>
      										</div>
      										<div class="amenities_img_icon d-block d-lg-none">
      											<img src="<?php echo base_url(); ?>assets/images/amenities/icon/coworkingspace.svg" class="img-fluid">
      										</div>
      										<div class="amenities_name d-none d-lg-block">
      											co-working space
      										</div>
      									</div>
      								</div>
      								<div class="swiper-slide">
      									<div class="amenities_data">
      										<div class="amenities_img_box">
      											<div class="amenities_img_icon d-none d-lg-block">
      												<img src="<?php echo base_url(); ?>assets/images/amenities/icon/fitnesscentre.svg" class="img-fluid">
      											</div>
      											<div class="amenities_name d-block d-lg-none">
      												fitness centre
      											</div>
      											<div class="amenities_img">
      												<img src="<?php echo base_url(); ?>assets/images/amenities/image/fitnesscentre.jpg" class="img-fluid">
      											</div>
      										</div>
      										<div class="amenities_img_icon d-block d-lg-none">
      											<img src="<?php echo base_url(); ?>assets/images/amenities/icon/fitnesscentre.svg" class="img-fluid">
      										</div>
      										<div class="amenities_name d-none d-lg-block">
      											fitness centre
      										</div>
      									</div>
      								</div>
      								<div class="swiper-slide">
      									<div class="amenities_data">
      										<div class="amenities_img_box">
      											<div class="amenities_img_icon d-none d-lg-block">
      												<img src="<?php echo base_url(); ?>assets/images/amenities/icon/hometheatre.svg" class="img-fluid">
      											</div>
      											<div class="amenities_name d-block d-lg-none">
      												home theatre
      											</div>
      											<div class="amenities_img">
      												<img src="<?php echo base_url(); ?>assets/images/amenities/image/hometheatre.jpg" class="img-fluid">
      											</div>
      										</div>
      										<div class="amenities_img_icon d-block d-lg-none">
      											<img src="<?php echo base_url(); ?>assets/images/amenities/icon/hometheatre.svg" class="img-fluid">
      										</div>
      										<div class="amenities_name d-none d-lg-block">
      											home theatre
      										</div>
      									</div>
      								</div>
      								<div class="swiper-slide">
      									<div class="amenities_data">
      										<div class="amenities_img_box">
      											<div class="amenities_img_icon d-none d-lg-block">
      												<img src="<?php echo base_url(); ?>assets/images/amenities/icon/kidsplayarea.svg" class="img-fluid">
      											</div>
      											<div class="amenities_name d-block d-lg-none">
      												kids play area
      											</div>
      											<div class="amenities_img">
      												<img src="<?php echo base_url(); ?>assets/images/amenities/image/kidsplayarea.jpg" class="img-fluid">
      											</div>
      										</div>
      										<div class="amenities_img_icon d-block d-lg-none">
      											<img src="<?php echo base_url(); ?>assets/images/amenities/icon/kidsplayarea.svg" class="img-fluid">
      										</div>
      										<div class="amenities_name d-none d-lg-block">
      											kids play area
      										</div>
      									</div>
      								</div>
      								<div class="swiper-slide">
      									<div class="amenities_data">
      										<div class="amenities_img_box">
      											<div class="amenities_img_icon d-none d-lg-block">
      												<img src="<?php echo base_url(); ?>assets/images/amenities/icon/massageroom.svg" class="img-fluid">
      											</div>
      											<div class="amenities_name d-block d-lg-none">
      												massage room
      											</div>
      											<div class="amenities_img">
      												<img src="<?php echo base_url(); ?>assets/images/amenities/image/massageroom.jpg" class="img-fluid">
      											</div>
      										</div>
      										<div class="amenities_img_icon d-block d-lg-none">
      											<img src="<?php echo base_url(); ?>assets/images/amenities/icon/massageroom.svg" class="img-fluid">
      										</div>
      										<div class="amenities_name d-none d-lg-block">
      											massage room
      										</div>
      									</div>
      								</div>
      								<div class="swiper-slide">
      									<div class="amenities_data">
      										<div class="amenities_img_box">
      											<div class="amenities_img_icon d-none d-lg-block">
      												<img src="<?php echo base_url(); ?>assets/images/amenities/icon/salon.svg" class="img-fluid">
      											</div>
      											<div class="amenities_name d-block d-lg-none">
      												salon
      											</div>
      											<div class="amenities_img">
      												<img src="<?php echo base_url(); ?>assets/images/amenities/image/salon.jpg" class="img-fluid">
      											</div>
      										</div>
      										<div class="amenities_img_icon d-block d-lg-none">
      											<img src="<?php echo base_url(); ?>assets/images/amenities/icon/salon.svg" class="img-fluid">
      										</div>
      										<div class="amenities_name d-none d-lg-block">
      											salon
      										</div>
      									</div>
      								</div>
      								<div class="swiper-slide">
      									<div class="amenities_data">
      										<div class="amenities_img_box">
      											<div class="amenities_img_icon d-none d-lg-block">
      												<img src="<?php echo base_url(); ?>assets/images/amenities/icon/sauna.svg" class="img-fluid">
      											</div>
      											<div class="amenities_name d-block d-lg-none">
      												sauna
      											</div>
      											<div class="amenities_img">
      												<img src="<?php echo base_url(); ?>assets/images/amenities/image/sauna.jpg" class="img-fluid">
      											</div>
      										</div>
      										<div class="amenities_img_icon d-block d-lg-none">
      											<img src="<?php echo base_url(); ?>assets/images/amenities/icon/sauna.svg" class="img-fluid">
      										</div>
      										<div class="amenities_name d-none d-lg-block">
      											sauna
      										</div>
      									</div>
      								</div>
      								<div class="swiper-slide">
      									<div class="amenities_data">
      										<div class="amenities_img_box">
      											<div class="amenities_img_icon d-none d-lg-block">
      												<img src="<?php echo base_url(); ?>assets/images/amenities/icon/sundeck.svg" class="img-fluid">
      											</div>
      											<div class="amenities_name d-block d-lg-none">
      												sun deck
      											</div>
      											<div class="amenities_img">
      												<img src="<?php echo base_url(); ?>assets/images/amenities/image/sundeck.jpg" class="img-fluid">
      											</div>
      										</div>
      										<div class="amenities_img_icon d-block d-lg-none">
      											<img src="<?php echo base_url(); ?>assets/images/amenities/icon/sundeck.svg" class="img-fluid">
      										</div>
      										<div class="amenities_name d-none d-lg-block">
      											sun deck
      										</div>
      									</div>
      								</div>
      								<div class="swiper-slide">
      									<div class="amenities_data">
      										<div class="amenities_img_box">
      											<div class="amenities_img_icon d-none d-lg-block">
      												<img src="<?php echo base_url(); ?>assets/images/amenities/icon/swimmingpool.svg" class="img-fluid">
      											</div>
      											<div class="amenities_name d-block d-lg-none">
      												swimming pool
      											</div>
      											<div class="amenities_img">
      												<img src="<?php echo base_url(); ?>assets/images/amenities/image/swimmingpool.jpg" class="img-fluid">
      											</div>
      										</div>
      										<div class="amenities_img_icon d-block d-lg-none">
      											<img src="<?php echo base_url(); ?>assets/images/amenities/icon/swimmingpool.svg" class="img-fluid">
      										</div>
      										<div class="amenities_name d-none d-lg-block">
      											swimming pool
      										</div>
      									</div>
      								</div>
      								<div class="swiper-slide">
      									<div class="amenities_data">
      										<div class="amenities_img_box">
      											<div class="amenities_img_icon d-none d-lg-block">
      												<img src="<?php echo base_url(); ?>assets/images/amenities/icon/totlotarea.svg" class="img-fluid">
      											</div>
      											<div class="amenities_name d-block d-lg-none">
      												tot lot area
      											</div>
      											<div class="amenities_img">
      												<img src="<?php echo base_url(); ?>assets/images/amenities/image/totlotarea.jpg" class="img-fluid">
      											</div>
      										</div>
      										<div class="amenities_img_icon d-block d-lg-none">
      											<img src="<?php echo base_url(); ?>assets/images/amenities/icon/totlotarea.svg" class="img-fluid">
      										</div>
      										<div class="amenities_name d-none d-lg-block">
      											tot lot area
      										</div>
      									</div>
      								</div>
      								<div class="swiper-slide">
      									<div class="amenities_data">
      										<div class="amenities_img_box">
      											<div class="amenities_img_icon d-none d-lg-block">
      												<img src="<?php echo base_url(); ?>assets/images/amenities/icon/walkingtrack.svg" class="img-fluid">
      											</div>
      											<div class="amenities_name d-block d-lg-none">
      												walking track
      											</div>
      											<div class="amenities_img">
      												<img src="<?php echo base_url(); ?>assets/images/amenities/image/walkingtrack.jpg" class="img-fluid">
      											</div>
      										</div>
      										<div class="amenities_img_icon d-block d-lg-none">
      											<img src="<?php echo base_url(); ?>assets/images/amenities/icon/walkingtrack.svg" class="img-fluid">
      										</div>
      										<div class="amenities_name d-none d-lg-block">
      											walking track
      										</div>
      									</div>
      								</div>
      							</div>
      							<div class="swiper-pagination"></div>
      						</div>
      					</div>
      				</div>
      			</div>
      		</div>
      	</section>

      	<section id="section6" class="section6">
      		<div class="my_container">
      			<div class="page_title_box text-center">
      				<div class="page_subtitle">
      					<img src="<?php echo base_url(); ?>assets/images/leaf.svg" class="img-fluid">
      					<span>floor PLANS</span>
      				</div>
      				<div class="page_title">
      					Luxurious spaces as grand in scale as your success
      				</div>
      			</div>
      			<div class="floor_plan">
      				<div class="zoom-image-container">
	      				<div class="floor_plan_details">
	      					<div class="rera_number d-none d-lg-block">
	      						RERA No : P51800005361
	      					</div>
	      					<div class="download_floor_plan">
	      						<!-- <a download href="<?php echo base_url(); ?>assets/images/Floor-Plan.pdf"><img src="<?php echo base_url(); ?>assets/images/download.svg" class="img-fluid"></a> -->

	      						<a href="javascript:void(0);" id="midpdf" class="mypdf" data-attr='midpdf' data-toggle="modal" data-target="#enqireModal">
				      				<img src="<?php echo base_url(); ?>assets/images/download.svg" class="img-fluid">
				      			</a>
				      			<a href="<?php echo base_url(); ?>assets/images/Floor-Plan.pdf" class='d-none' id='midpdf_click' download>clickme</a>

	      					</div>


	      					<div class="floor_plan_img zoom-image" data-image="<?php echo base_url(); ?>assets/images/floorplan.jpg">
	      						<img src="<?php echo base_url(); ?>assets/images/floorplan.jpg" class="img-fluid">
	      					</div>
	      				</div>
      				</div>
      			</div>
      			<div class="download_floorplan_cta">
      				<a href="javascript:void(0);" id="midpdf1" class="form_cta mypdf" data-attr='midpdf1' data-toggle="modal" data-target="#enqireModal">
	      				<span>Download Floor Plan</span>
	      			</a>
	      			<a href="<?php echo base_url(); ?>assets/images/Floor-Plan.pdf" class='d-none' id='midpdf_click1' download>clickme</a>
      			</div>
      			<div class="rera_number d-block d-lg-none">
      				RERA No : P51800005361
      			</div>
      			<div class="floor_disclaimer">
      				Disclaimer : The plans, specification, elevation, conceptual designs, visuals, images, dimensions, photograph, furniture, fixtures, amenities, facilities, etc. are strictly provided for representation purpose only and are subject to requisite approvals from the competent authorities unless otherwise stated herein. This does not constitute an offer and/or contract of any nature between the company and the prospective purchaser/customers has independently satisfied himself before any conclusive decision. All brand names and trademarks stands reserved. The carpet area of the Apartment(s) mentioned is as per definition mentioned In ‘Maharashtra Real estate Regulatory Authority (MahaRERA).
      			</div>
      		</div>
      	</section>

      	<section id="section7" class="section7">
      		<div class="my_container">
      			<div class="page_title_box">
      				<div class="page_subtitle">
      					<img src="<?php echo base_url(); ?>assets/images/leaf.svg" class="img-fluid">
      					<span>LOCATION</span>
      				</div>
      				<div class="page_title">
      					Off New Link Road, Andheri
      				</div>
      			</div>
      			<div class="section7_desc">
      				An ideal location with seamless connectivity through various modes of transit for maximum comfort. It is also dotted with leading educational institutes, healthcare, leisure and world-class shopping avenues.
      			</div>
      			<div class="row">
      				<div class="col-lg-8">
      					<div class="locationimg">
      						<img src="<?php echo base_url(); ?>assets/images/locationimg.png" class="img-fluid">
      					</div>
      					<div class="viewgooglemaps">
      						<a href="https://www.google.com/maps/place/Transcon+Triumph+-+Apartments+in+Andheri+west/@19.1356632,72.8219838,14.5z/data=!4m5!3m4!1s0x0:0x52fdd4275b0cfd13!8m2!3d19.137351!4d72.8340716" target="_blank">
      							View on Google Maps
      						</a>
      					</div>
      				</div>
      				<div class="col-lg-4">
      					<div id="locationmain">
      					   	<div class="accordion" id="location">
      					        <div class="card">
      					            <div class="card-header" id="locationhead1">
      					               	<a href="#" class="btn btn-header-link collapsed" data-toggle="collapse" data-target="#location1"
      					                  aria-expanded="true" aria-controls="location1">Connectivity</a>
      					            </div>
      					            <div id="location1" class="collapse" aria-labelledby="locationhead1" data-parent="#location">
      					               	<div class="card-body">
      					                  	<ul>
      					                  		<li>
      					                  			<span class="location_name">Andheri Station</span>
      					                  			<span class="location_distance">3.6 km*</span>
      					                  		</li>
      					                  		<li>
      					                  			<span class="location_name">D N Nagar Metro Station</span>
      					                  			<span class="location_distance">2.3 km*</span>
      					                  		</li>
      					                  		<li>
      					                  			<span class="location_name">Western Express Highway</span>
      					                  			<span class="location_distance">3.3 km*</span>
      					                  		</li>
      					                  		<li>
      					                  			<span class="location_name">Veera Desai Road</span>
      					                  			<span class="location_distance">1.2 km*</span>
      					                  		</li>
      					                  		<li>
      					                  			<span class="location_name">Lokhandwala Complex</span>
      					                  			<span class="location_distance">1.7 km*</span>
      					                  		</li>
      					                  	</ul>
      					               	</div>
      					            </div>
      					        </div>
      					        <div class="card">
      					            <div class="card-header" id="locationhead2">
      					               	<a href="#" class="btn btn-header-link collapsed" data-toggle="collapse" data-target="#location2"
      					                  aria-expanded="true" aria-controls="location2">Education Institutions</a>
      					            </div>
      					            <div id="location2" class="collapse" aria-labelledby="locationhead2" data-parent="#location">
      					               	<div class="card-body">
      					                  	<ul>
      					                  		<li>
      					                  			<span class="location_name">Oriental College</span>
      					                  			<span class="location_distance">850 m*</span>
      					                  		</li>
      					                  		<li>
      					                  			<span class="location_name">JBCN International School</span>
      					                  			<span class="location_distance">1.3 km*</span>
      					                  		</li>
      					                  		<li>
      					                  			<span class="location_name">Valia College Bhavnagar</span>
      					                  			<span class="location_distance">2.1 km*</span>
      					                  		</li>
      					                  		<li>
      					                  			<span class="location_name">Billabong International School</span>
      					                  			<span class="location_distance">2.4 km*</span>
      					                  		</li>
      					                  		<li>
      					                  			<span class="location_name">Rajhans Vidyalaya</span>
      					                  			<span class="location_distance">3.1 km*</span>
      					                  		</li>
      					                  	</ul>
      					               	</div>
      					            </div>
      					        </div>
      					        <div class="card">
      					            <div class="card-header" id="locationhead3">
      					               	<a href="#" class="btn btn-header-link collapsed" data-toggle="collapse" data-target="#location3"
      					                  aria-expanded="true" aria-controls="location3">Leisure & Social Life</a>
      					            </div>
      					            <div id="location3" class="collapse" aria-labelledby="locationhead3" data-parent="#location">
      					               	<div class="card-body">
      					                  	<ul>
      					                  		<li>
      					                  			<span class="location_name">Lord Of The Drinks</span>
      					                  			<span class="location_distance">350 m*</span>
      					                  		</li>
      					                  		<li>
      					                  			<span class="location_name">Irish House</span>
      					                  			<span class="location_distance">1.5 km*</span>
      					                  		</li>
      					                  		<li>
      					                  			<span class="location_name">The Little Door</span>
      					                  			<span class="location_distance">270 m*</span>
      					                  		</li>
      					                  		<li>
      					                  			<span class="location_name">Bombay Cocktail Bar</span>
      					                  			<span class="location_distance">190 m*</span>
      					                  		</li>
      					                  		<li>
      					                  			<span class="location_name">Tap Resto Bar</span>
      					                  			<span class="location_distance">950 m*</span>
      					                  		</li>
      					                  	</ul>
      					               	</div>
      					            </div>
      					        </div>
      					        <div class="card">
      					            <div class="card-header" id="locationhead4">
      					               <a href="#" class="btn btn-header-link collapsed" data-toggle="collapse" data-target="#location4"
      					                  aria-expanded="true" aria-controls="location4">Malls, Entertainment & Supermarket</a>
      					            </div>
      					            <div id="location4" class="collapse" aria-labelledby="locationhead4" data-parent="#location">
      					               	<div class="card-body">
      					                  	<ul>
      					                  		<li>
      					                  			<span class="location_name">Infinity Mall</span>
      					                  			<span class="location_distance">800 m*</span>
      					                  		</li>
      					                  		<li>
      					                  			<span class="location_name">PVR Theatre</span>
      					                  			<span class="location_distance">600 m*</span>
      					                  		</li>
      					                  		<li>
      					                  			<span class="location_name">Citi Mall</span>
      					                  			<span class="location_distance">600 m*</span>
      					                  		</li>
      					                  		<li>
      					                  			<span class="location_name">The Club</span>
      					                  			<span class="location_distance">3.1 km*</span>
      					                  		</li>
      					                  		<li>
      					                  			<span class="location_name">Cinépolis</span>
      					                  			<span class="location_distance">1.5 km*</span>
      					                  		</li>
      					                  	</ul>
      					               	</div>
      					            </div>
      					        </div>
      					        <div class="card">
      					            <div class="card-header" id="locationhead5">
      					               <a href="#" class="btn btn-header-link collapsed" data-toggle="collapse" data-target="#location5"
      					                  aria-expanded="true" aria-controls="location5">Healthcare</a>
      					            </div>
      					            <div id="location5" class="collapse" aria-labelledby="locationhead5" data-parent="#location">
      					               	<div class="card-body">
      					                  	<ul>
      					                  		<li>
      					                  			<span class="location_name">Bellevue Hospital</span>
      					                  			<span class="location_distance">1.8 km*</span>
      					                  		</li>
      					                  		<li>
      					                  			<span class="location_name">Kokilaben Hospital</span>
      					                  			<span class="location_distance">2 km*</span>
      					                  		</li>
      					                  		<li>
      					                  			<span class="location_name">Reliance Energy Ltd Hospital</span>
      					                  			<span class="location_distance">3.6 km*</span>
      					                  		</li>
      					                  		<li>
      					                  			<span class="location_name">BSES MG Hospital</span>
      					                  			<span class="location_distance">3.8 km*</span>
      					                  		</li>
      					                  	</ul>
      					               	</div>
      					            </div>
      					        </div>
      					    </div>
      					</div>
      				</div>
      			</div>
      		</div>
      	</section>

      	<section id="section8" class="section8">
      		<div class="my_container">
      			<div class="page_title_box text-center">
      				<div class="page_subtitle white">
      					<img src="<?php echo base_url(); ?>assets/images/wleaf.svg" class="img-fluid">
      					<span>Legacy</span>
      				</div>
      				<div class="page_title white">
      					Transcon – Art of Transforming
      				</div>
      			</div>
      			<div class="section8_desc white text-center">
      				Since 1985, Transcon Group has been creating iconic landmarks in the Mumbai Metropolitan Region. We thrive on creating comprehensive lifestyles beyond bricks and mortar to add value to your life. We ensure that our living spaces inspired by nature help you transform the way you live, feel and express to promote healthy community living.
      			</div>
      			<div class="aboutusskyline text-center">
      				<img src="<?php echo base_url(); ?>assets/images/aboutusskyline.png" class="img-fluid">
      			</div>
      		</div>
      	</section>

      	<section id="section9" class="section9">
      		<div class="container-fluid">
      			<div class="row">
      				<div class="col-lg-6 pl-lg-0">
      					<div class="contact_form_section">
      						<div class="page_title_box">
      							<div class="page_subtitle">
      								<img src="<?php echo base_url(); ?>assets/images/leaf.svg" class="img-fluid">
      								<span>enquire</span>
      							</div>
      							<div class="page_title">
      								Learn more about Luminati
      							</div>
      						</div>
      						<div class="section9_desc">
      							Fill in the below form and we will get in touch with you soon.
      						</div>
      						<div class="contact_form_box">
								<form id="enquireform" class="" action="javascript:void(0);" novalidate="" autocomplete="off">
      								<div class="form-group form_width">
                                        <input id="enquire_name" name="enquire_name" type="text" class="form-control txtNumeric" autocomplete="off" required="">
                                        <span for="enquire_name">Name*</span>
                                        <div class="error" id="enquire_name_error"></div>
                                    </div>
      								<div class="form-group form_width">
                                        <input id="enquire_phone" name="enquire_phone" type="tel" class="form-control" autocomplete="off" required="" onkeypress="return event.charCode >=48 && event.charCode<=57" maxlength="10" onkeypress="return isNumberKey(event)">
                                        <span for="enquire_phone">Phone No*</span>
                                        <div class="error" id="enquire_phone_error"></div>
                                    </div>
      								<div class="form-group form_width">
                                        <input id="enquire_email" name="enquire_email" type="text" class="form-control" autocomplete="off" required="">
                                        <span for="enquire_email">Email ID*</span>
                                        <div class="error" id="enquire_email_error"></div>
                                    </div>
      								<div class="form-group form_width">
                                        <input id="enquire_location" name="enquire_location" type="text" class="form-control" autocomplete="off" required="">
                                        <span for="enquire_location">Location</span>
                                        <div class="error" id="enquire_location_error"></div>
                                    </div>
      								<div class="form-group form_width form_width_full">
                                        <input id="enquire_message" name="enquire_message" type="text" class="form-control" autocomplete="off" required="">
                                        <span for="enquire_message">Message</span>
                                        <div class="error" id="enquire_message_error"></div>
                                    </div>
                                    <div class="w-100 submit_btn">
                                        <button type="submit" class="form_cta" id="makemedisable"><span>Submit</span></button>
                                        <div class="ajaxsuccess"></div>
                                        <div id="loader" class="text-center" style="display: none;">
                                            <img src="<?php echo base_url(); ?>assets/images/ajaxloader.gif" alt="">
                                        </div>
                                    </div>
      							</form>
      						</div>
      					</div>
      					<div class="contact_details">
      						<div class="container-fluid">
      							<div class="row">
      								<div class="col-sm-6">
      									<div class="contact_detail">
      										<img src="<?php echo base_url(); ?>assets/images/email.svg" class="img-fluid">
      										<span><a href="mailto:triumphsales@transcon.in">triumphsales@transcon.in</a></span>
      									</div>
      									<div class="contact_detail">
      										<img src="<?php echo base_url(); ?>assets/images/call.svg" class="img-fluid">
      										<span><a href="tel:7219411777">7219411777</a></span>
      									</div>
      									<div class="contact_detail">
      										<img src="<?php echo base_url(); ?>assets/images/reraicon.svg" class="img-fluid">
      										<span>RERA No : P51800005361</span>
      									</div>
      									<div class="contact_detail">
      										<img src="<?php echo base_url(); ?>assets/images/rera.svg" class="img-fluid">
      										<a href="http://transcon.in/" target="_blank"><span>transcon.in</span></a>
      									</div>
      								</div>
      								<div class="col-sm-6">
      									<div class="contact_detail">
      										<img src="<?php echo base_url(); ?>assets/images/address.svg" class="img-fluid">
      										<span>
      											Transcon Triumph, Next to Oberoi Springs, Opp. Tanishq Showroom, Off New Link Road, Andheri (W), Mumbai - 400053
      										</span>
      									</div>
      									<div class="see_map">
      										<a href="https://www.google.com/maps/place/Transcon+Triumph+-+Apartments+in+Andheri+west/@19.1356632,72.8219838,14.5z/data=!4m5!3m4!1s0x0:0x52fdd4275b0cfd13!8m2!3d19.137351!4d72.8340716" target="_blank">See on Maps</a>
      									</div>
      								</div>
      							</div>
      						</div>
      					</div>
      				</div>
      				<div class="col-lg-6 p-0">
      					<div class="contact_img">
      						<img src="<?php echo base_url(); ?>assets/images/contact_img.jpg" class="img-fluid">
      					</div>
      				</div>
      			</div>
      		</div>
      	</section>

      	<section class="enqire_modal">
      		<div class="modal" id="enqireModal">
      		  	<div class="modal-dialog modal-dialog-centered">
      		    	<div class="modal-content">
	      		      	<div class="modal-header">
	      		        	<div class="modal-title">Transcon Triumph</div>
	      		        	<div class="modal-desc">Fill in the below form and we will get in touch with you soon.</div>
	      		        	<button type="button" class="close" data-dismiss="modal">&times;</button>
	      		      	</div>
	      		      	<div class="modal-body">
							<form id="enquireform-modal" class="" action="javascript:void(0);" novalidate="" autocomplete="off">
  								<div class="form-group form_width form_width_full">
                                    <input id="modal_enquire_name" name="enquire_name" type="text" class="form-control txtNumeric" autocomplete="off" required="">
                                    <span for="enquire_name">Name*</span>
                                    <div class="error" id="modal_enquire_name_error"></div>
                                </div>
  								<div class="form-group form_width form_width_full">
                                    <input id="modal_enquire_phone" name="enquire_phone" type="tel" class="form-control" autocomplete="off" required="" onkeypress="return event.charCode >=48 && event.charCode<=57" maxlength="10" onkeypress="return isNumberKey(event)">
                                    <span for="enquire_phone">Phone No*</span>
                                    <div class="error" id="modal_enquire_phone_error"></div>
                                </div>
  								<div class="form-group form_width form_width_full">
                                    <input id="modal_enquire_email" name="enquire_email" type="text" class="form-control" autocomplete="off" required="">
                                    <span for="enquire_email">Email ID*</span>
                                    <div class="error" id="modal_enquire_email_error"></div>
                                </div>
                                <div class="w-100 submit_btn">
                                    <button type="submit" class="form_cta" id="modal_makemedisable"><span>Submit</span></button>
                                    <div class="modal_ajaxsuccess"></div>
                                    <div id="modal_loader" class="text-center" style="display: none;">
                                        <img src="<?php echo base_url(); ?>assets/images/ajaxloader.gif" alt="">
                                    </div>
                                </div>
  							</form>
	      		      	</div>
      		    	</div>
      		  	</div>
      		</div>
      	</section>

      	<section class="sticky_box">
      		<div class="top_cta">
      			<a class="scrolls enquiresticky" href="#section9"><span>Enquire</span></a>
      			<a href="tel:7219411777"><img src="<?php echo base_url(); ?>assets/images/phone.svg" class="img-fluid"></a>
      			<a href="javascript:void(0);" id="rightpdf" class="mypdf" data-attr='rightpdf' data-toggle="modal" data-target="#enqireModal">
      				<img src="<?php echo base_url(); ?>assets/images/downloadblack.svg" class="img-fluid">
      			</a>
      			<a href="<?php echo base_url(); ?>assets/images/Luminati_E-Brochure.pdf" class='d-none' id='rightpdf_click' download>clickme</a>
      		</div>
      		<div class="whatsapp_cta">
      			<a class="scrolls" target="_blank" href="//api.whatsapp.com/send?phone=917039550739&text=Step Into The Unique World Of Luminati... Ultra-spacious 4BHK ready to move-in homes at Andheri West."><img src="<?php echo base_url(); ?>assets/images/whatsapp.svg" class="img-fluid"></a>
      		</div>
      	</section>

      	<section class="mobile_sticky_box">
      		<div class="whatsapp_cta">
      			<a class="scrolls" target="_blank" href="//api.whatsapp.com/send?phone=917039550739&text=Step Into The Unique World Of Luminati... Ultra-spacious 4BHK ready to move-in homes at Andheri West."><img src="<?php echo base_url(); ?>assets/images/whatsapp.svg" class="img-fluid"></a>
      		</div>
      		<div class="mob_sticky">
      			<a href="tel:7219411777"><img src="<?php echo base_url(); ?>assets/images/phone-black.svg" class="img-fluid"></a>
      			<a class="scrolls mobile_enquiresticky" href="#section9"><span>Enquire</span></a>
      			<a href="javascript:void(0);" id="rightpdf1" class="mypdf" data-attr='rightpdf1' data-toggle="modal" data-target="#enqireModal">
      				<img src="<?php echo base_url(); ?>assets/images/download-black.svg" class="img-fluid">
      			</a>
      			<a href="<?php echo base_url(); ?>assets/images/Luminati_E-Brochure.pdf" class='d-none' id='rightpdf_click1' download>clickme</a>
      		</div>
      	</section>
      	<!-- PAGE CONTENT ENDS HERE -->

      	<!-- WEBSITE FOOTER STARTS HERE -->
      	<?php include("common/footer.php");?>
      	<!-- WEBSITE FOOTER ENDS HERE -->
      	<?php include("common/js-scripts.php");?>
      	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/form.js?<?php echo date("YmdHis");?>"></script>
   	</body>
</html>