<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$page = 'thankyou';

// print_r($_SESSION);

if(isset($_SESSION['check_session']) && !empty($_SESSION['check_session'])) {
	unset ($_SESSION["check_session"]);
?>
<!DOCTYPE html>
<html>
   	<head>
      	<title>Transcon Triumph - Thank You</title>
      	<?php include("common/meta-content.php");?>
      	<?php include("common/css-scripts.php");?>
      	<style type="text/css">
      		.error{color: red;}

      		body {
      		    padding-top: 0;
      		}

      		footer {
      			margin: 0;
      		}

      		.tysection1 {
      		    margin: 0;
      		    padding: 1.25em 0;
      		    background: url(assets/images/parallax.jpg);
      		    background-attachment: fixed;
      		    background-position: center;
      		    height: calc(100vh - 3.500em);
      		}

      		.logo {
      			margin: 0;
      			display: block;
      		}

      		.ty_main {
      			height: 80%;
      		}

      		.ty_text {
      		    text-align: center;
      		    font-size: 1.5em;
      		    padding: 2.917em 0;
      		}

      		.ty_title {
      		    text-align: center;
      		    font-size: 5.938em;
      		    font-family: 'PlayfairDisplay', serif;
      		    line-height: normal;
      		}
      		@media(max-width:  834px) {
      			.tysection1 {
      				background: #e0f4f3;
      			}
      		}
      	</style>
   	</head>
   	<body>
      	<!-- WEBSITE HEADER STARTS HERE -->
      	<?php //include("common/header.php");?>
      	<!-- WEBSITE HEADER ENDS HERE -->

      	<!-- PAGE CONTENT STARTS HERE -->
      		<section class="tysection1">
      			<div class="my_container h-100">
		  			<div class="logo text-center">
		  				<a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/images/logo.png" class="img-fluid"></a>
		  			</div>
		  			<div class="ty_main d-flex flex-wrap flex-column justify-content-center align-content-center">
		  				<div class="ty_title">
			  				Thank You!
			  			</div>
			  			<div class="ty_text">
			  				Thank you for expressing interest on Transcon Developers. <br>
			  				Our team will get in touch with you shortly. 
			  			</div>
			  			<div class="text-center">
			  				<a href="<?php echo base_url(); ?>" class="form_cta"><span>Go back to Homepage</span></a>
			  			</div>
		  			</div>
		  		</div>
      		</section>
      	<!-- PAGE CONTENT ENDS HERE -->

      	<!-- WEBSITE FOOTER STARTS HERE -->
      	<?php include("common/footer.php");?>
      	<!-- WEBSITE FOOTER ENDS HERE -->
      	<?php include("common/js-scripts.php");?>
   	</body>
</html>
<?php } else {
	header('Location: https://transcon-triumph.com');
}
?>